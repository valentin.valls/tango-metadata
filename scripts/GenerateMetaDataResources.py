#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''

Generation of tango resource files for MetaData servers.
'''
__author__ = "Holger.Witsch@esrf.fr"
__date__ = "Jan 2019"
__version__ = 1.0

import os #@UnusedImport
import sys
import time
from optparse import OptionParser


Resources = '''
#
# Resources, created by {program} on {date}
#

#---------------------------------------------------------
# SERVER MetadataManager/{BL}, MetadataManager device declaration
#---------------------------------------------------------

MetadataManager/{BL}/DEVICE/MetadataManager: "{BL}/metadata/mgr"


# --- {BL}/metadata/mgr properties

{BL}/metadata/mgr->beamlineID: {BL}
{BL}/metadata/mgr->dataFolderPattern: "{{dataRoot}}/{{sampleName}}/{{datasetName}}"
{BL}/metadata/mgr->globalHDFfiles: "{{dataRoot}}/{{datasetName}}.h5"
{BL}/metadata/mgr->labels: "definition = test"
{BL}/metadata/mgr->metaExperimentDevice: "{BL}/metadata/exp"
{BL}/metadata/mgr->parameters: InstrumentSource_mode


# --- {BL}/metadata/mgr attribute properties

{BL}/metadata/mgr/dataFolder->__value: ""
{BL}/metadata/mgr/InstrumentSource_mode->__value: bunch_later
{BL}/metadata/mgr/metadataFile->__value: ""
{BL}/metadata/mgr/datasetName->__value: ""

#---------------------------------------------------------
# CLASS MetadataManager properties
#---------------------------------------------------------

CLASS/MetadataManager->API_KEY: elogbook-be70ac55-fd08-4840-9b29-b73262958ca8
CLASS/MetadataManager->authenticationPlugin: db
CLASS/MetadataManager->icatplus_server: "https://icatplus.esrf.fr"
CLASS/MetadataManager->password: reader
CLASS/MetadataManager->port: 443
CLASS/MetadataManager->queueName: "/queue/icatIngest"
CLASS/MetadataManager->queueURLs: bcu-mq-01.esrf.fr:61613
CLASS/MetadataManager->server: icat.esrf.fr
CLASS/MetadataManager->username: reader

# CLASS MetadataManager attribute properties




#---------------------------------------------------------
# SERVER MetaExperiment/{BL}, MetaExperiment device declaration
#---------------------------------------------------------

MetaExperiment/{BL}/DEVICE/MetaExperiment: "{BL}/metadata/exp"


# --- {BL}/metadata/exp properties

{BL}/metadata/exp->beamlineID: {BL}

# --- {BL}/metadata/exp attribute properties

{BL}/metadata/exp/dataRoot->__value: "/data/visitor/"
{BL}/metadata/exp/proposal->__value: "please enter"
{BL}/metadata/exp/sample->__value: "please enter"

#---------------------------------------------------------
# CLASS MetaExperiment properties
#---------------------------------------------------------

CLASS/MetaExperiment->queueURLs: bcu-mq-01.esrf.fr:61613

# CLASS MetaExperiment attribute properties


'''


def main(args):

    print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    print("        MetaData TANGO DEVICE SERVER RESOURCES GENERATOR        ")
    print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n\n")

    parser = OptionParser(description = 'MetaData TANGO DEVICE SERVER RESOURCES GENERATOR')
    parser.add_option('-d', action = "store_true", default = False, help = 'debug', metavar = 'DEBUG')
    parser.add_option('-b', '--beamline', dest = 'bl_name',
                      help = 'the beam line name',
                      metavar = 'BLNAME')
    (options, args) = parser.parse_args()
    args = args

    if not options.bl_name:
        parser.error('No beam line name given!')
        sys.exit()
    bl_name = options.bl_name
    if bl_name.startswith("d"):
        parser.error('Bipole beam line names must start with "bm"!')
        sys.exit()


    debug = options.d
    if debug: print("Options:", options, file=sys.stderr)

    try:
        # Opens a file named according to BL number.
        res_file_name = "MetaData_{0}.tango".format(bl_name.upper())
        rf = open(res_file_name, mode = 'w+')
    except:
        sys.excepthook(sys.exc_info()[0],
                       sys.exc_info()[1],
                       sys.exc_info()[2])
        rf.close()
    else:
        print("Tango resources for {0} MetaData will be generated in file {1}".format(options.bl_name, res_file_name))



    rf.write(Resources.format(BL=bl_name, date=time.asctime(time.localtime(time.time())),program=os.path.basename(sys.argv[0])))
    rf.close()
    print()

if __name__ == "__main__":
    main(sys.argv)



