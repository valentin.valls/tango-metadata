#! /bin/bash

. blissrc

BASE_FOLDER=\${BLISSADM}/applications/${python.package.name}

APPLICATION=${python.package.name}/MetadataManager.py

cd \${BASE_FOLDER}

exec python \${APPLICATION} "$@"