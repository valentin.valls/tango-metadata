import PyTango
import time

d1 = PyTango.DeviceProxy("test/meta/1")
d2 = PyTango.DeviceProxy("test/ingest/1")

print("device: " + d1.name())
print("device: " + d2.name())
print((d2.get_property('parameters')))
print((d2.get_attribute_list()))
time.sleep(5)

print("Resetting ...")
d2.Reset()
print(("State: " + str(d2.status()) + " - " + str(d2.state())))
time.sleep(5)

print("New proposal 'test0'")
d1.proposal = "test0"
print("Proposal: " + d2["proposal"].value)
print(("State: " + str(d2.status()) + " - " + str(d2.state())))
time.sleep(5)

print("New Sample 'sample1'")
d1.sample = "sample1"
print("Sample: " + d2["sampleName"].value)
time.sleep(5)

print("Set sample parameters")
d2.SetSampleParameters(["key1","value1","key2","value2"])
print((d2["sampleParameters"].value))
time.sleep(5)
d2.SetSampleParameters(["key1","newvalue!!","key3","value3"])
print((d2["sampleParameters"].value))
time.sleep(5)

print("New Dataset 'dataset1'")
d2.datasetName = "dataset1"
print(("State: " + str(d2.status()) + " - " + str(d2.state())))
print(("Proposed data folder   " + d2["dataFolder"].value))
print(("Proposed metadata file " + d2["metadataFile"].value))
time.sleep(5)

print("direct parameter set")
time.sleep(5)
d2.filter = "my filter"
time.sleep(5)

print("Start dataset")
d2.StartDataset()
print(("State: " + str(d2.status()) + " - " + str(d2.state())))
time.sleep(5) 

print("direct parameter set")
time.sleep(5)
d2.filter = "my new filter"
time.sleep(5)

print("recording files")
time.sleep(5)
d2.lastDatafile = "file1"
time.sleep(5)
d2.lastDatafile= "file2"
time.sleep(5)

print("Ending dataset")
d2.EndDataset()
print(("State: " + str(d2.status()) + " - " + str(d2.state())))
time.sleep(5)

