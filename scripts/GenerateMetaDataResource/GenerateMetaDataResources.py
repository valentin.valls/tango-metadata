#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''

Generation of tango resource files for MetaData servers.
'''
__author__ = "Holger.Witsch@esrf.fr"
__date__ = "Dec 2019"
__version__ = 2.0

import os 
import sys
from optparse import OptionParser
import re
import shutil
from tempfile import mkstemp


def sed(strings_to_replace, replaced_by, ref_file, output_file):
    """
    Reads a file_name and overwrite it.
    In each line, string_to_replace is replaced with replace_by
    """

    fin = open(ref_file, 'r')
    fout = open(output_file, 'w')


    for line in fin:
        # first bl_name
        out0 = re.sub(strings_to_replace[0], replaced_by[0], line)
        # then member_name
        out1 = re.sub(strings_to_replace[1], replaced_by[1], out0)
        # last personal_name
        out2 = re.sub(strings_to_replace[2], replaced_by[2], out1)
        fout.write(out2)

    fin.close()
    fout.close()

def main(args):
    
    print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
    print("        MetaData TANGO DEVICE SERVER RESOURCES GENERATOR        ")
    print("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n")
    
    parser = OptionParser(description = 'MetaData TANGO DEVICE SERVER RESOURCES GENERATOR', usage="usage: %prog -b BL_NAME [options]")
    parser.add_option('-b', '--beamline', dest = 'bl_name',
                      help = 'the beam line name')
    parser.add_option('-m', '--member_name', dest = 'member_name',
                      help = 'the member part of the three part device (domain/family/member)')
    parser.add_option('-p', '--personal_name', dest = 'personal_name',
                      help = 'the personal name of the Device Server')
    (options, args) = parser.parse_args()
    args = args
    # in case we have more options
    if len(args) > 0:
        parser.print_help()
        sys.exit(0)
        
    if not options.bl_name:
        print('No beamline name given!')
        parser.print_help()
        sys.exit(1)
    
    beamline_name=format(options.bl_name.lower())
    if beamline_name.startswith("d"):
        parser.error('Bendingmagnet beam line names must start with "bm"!')
        sys.exit(1)
        
    # if no member_name take bl_name
    if not options.member_name:
        member_name = beamline_name
    else:
        member_name = options.member_name
    
    # if no personal_name we use capital beamline name
    if not options.personal_name:
        personal_name = beamline_name.upper()
    else:
        personal_name = options.personal_name
     
    # fill up the vector
    strings_to_replace = ['BL', 'MEMBER', 'PERSONAL']
    replaced_by = [beamline_name, member_name, personal_name]
           
    # Get Path for reference files
    path_for_reference_files = os.path.dirname(os.path.realpath(__file__))        
    
    # Output file with personal name as sign
    res_file_name = "MetadataManager_{0}.tango".format(personal_name)
    ref_file_name = path_for_reference_files + "/MetadataManager.ref"    
    sed(strings_to_replace, replaced_by, ref_file_name, res_file_name)        
    print("Tango resources for MetadataManager with Personal name {0} to be found at:\n{1} in current directory".format(personal_name, res_file_name))

    # now deal with MetaExperiment
    res_file_name = "MetaExperiment_{0}.tango".format(personal_name)
    ref_file_name = path_for_reference_files + "/MetaExperiment.ref"    
    sed(strings_to_replace, replaced_by, ref_file_name, res_file_name)    
    print("Tango resources for MetaExperiment with Personal name {0} to be found at:\n{1} in current directory".format(personal_name, res_file_name))


if __name__ == "__main__":
    main(sys.argv)



