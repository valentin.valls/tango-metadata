import unittest
from pprint import pprint
import sys
from metadata_manager.MetaExperiment import MetaExperiment
from metadata_manager.MetaExperiment import MetaExperimentClass
import PyTango.client


class TestMetaExperiment(unittest.TestCase):
    data_root = "/tmp/data"
    proposal = "ID000000"
    sample = "testSample"

    DEFAULT_SAMPLE_NAME = "please enter"
    DEFAULT_DATA_ROOT = "/data/visitor/"

    exp_device = "id00/meta_exp/id00"

    def test_init_metadata_experiment(self):
        try:
            meta_experiment = PyTango.client.Device(self.exp_device)
            pprint(meta_experiment.State())
            assert meta_experiment.state() is not None
        except Exception:
            print("Unexpected error:", sys.exc_info()[0])
            raise

    def test_set_data_root_metadata_experiment(self):
        try:
            meta_experiment = PyTango.client.Device(self.exp_device)
            meta_experiment.dataRoot = self.data_root
            self.assertEqual(meta_experiment.dataRoot, self.data_root)
        except Exception:
            print("Unexpected error:", sys.exc_info()[0])
            raise

    def test_set_proposal_metadata_experiment(self):
        try:
            meta_experiment = PyTango.client.Device(self.exp_device)
            meta_experiment.proposal = self.proposal
            self.assertEqual(meta_experiment.proposal, self.proposal)
        except Exception:
            print("Unexpected error:", sys.exc_info()[0])
            raise

    def test_set_sample_metadata_experiment(self):
        try:
            meta_experiment = PyTango.client.Device(self.exp_device)
            meta_experiment.sample = self.sample
            self.assertEqual(meta_experiment.sample, self.sample)
        except Exception:
            print("Unexpected error:", sys.exc_info()[0])
            raise

    def test_full_metadata_experiment(self):
        try:
            meta_experiment = PyTango.client.Device(self.exp_device)
            meta_experiment.proposal = self.proposal
            meta_experiment.dataRoot = self.data_root
            meta_experiment.sample = self.sample
            self.assertEqual(meta_experiment.dataRoot, self.data_root)
            self.assertEqual(meta_experiment.proposal, self.proposal)
            self.assertEqual(meta_experiment.sample, self.sample)
        except Exception:
            print("Unexpected error:", sys.exc_info()[0])
            raise

    def test_change_proposal_metadata_experiment(self):
        try:
            meta_experiment = PyTango.client.Device(self.exp_device)
            meta_experiment.proposal = self.proposal
            meta_experiment.dataRoot = self.data_root
            meta_experiment.sample = self.sample
            self.assertEqual(meta_experiment.dataRoot, self.data_root)
            self.assertEqual(meta_experiment.proposal, self.proposal)
            self.assertEqual(meta_experiment.sample, self.sample)
            meta_experiment.proposal = self.proposal
            self.assertEqual(meta_experiment.sample, self.DEFAULT_SAMPLE_NAME)
            self.assertEqual(meta_experiment.dataRoot, self.DEFAULT_DATA_ROOT)

        except Exception:
            print("Unexpected error:", sys.exc_info()[0])
            raise
if __name__ == '__main__':
    unittest.main()
